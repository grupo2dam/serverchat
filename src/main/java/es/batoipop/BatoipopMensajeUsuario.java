package es.batoipop;

import java.io.Serializable;
import java.util.Date;

public class BatoipopMensajeUsuario implements Serializable {
    private int id;
    private BatoipopUsuario batoipopUsuarioByUsuarioReceptor;
    private BatoipopUsuario batoipopUsuarioByUsuarioEmisor;
    private String contenido;
    private String fecha;

    public BatoipopMensajeUsuario() {
    }

    public BatoipopMensajeUsuario(int id, BatoipopUsuario batoipopUsuarioByUsuarioReceptor, BatoipopUsuario batoipopUsuarioByUsuarioEmisor, String contenido, String fecha) {
        this.id = id;
        this.batoipopUsuarioByUsuarioReceptor = batoipopUsuarioByUsuarioReceptor;
        this.batoipopUsuarioByUsuarioEmisor = batoipopUsuarioByUsuarioEmisor;
        this.contenido = contenido;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public BatoipopUsuario getBatoipopUsuarioByUsuarioReceptor() {
        return batoipopUsuarioByUsuarioReceptor;
    }

    public BatoipopUsuario getBatoipopUsuarioByUsuarioEmisor() {
        return batoipopUsuarioByUsuarioEmisor;
    }

    public String getContenido() {
        return contenido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBatoipopUsuarioByUsuarioReceptor(BatoipopUsuario batoipopUsuarioByUsuarioReceptor) {
        this.batoipopUsuarioByUsuarioReceptor = batoipopUsuarioByUsuarioReceptor;
    }

    public void setBatoipopUsuarioByUsuarioEmisor(BatoipopUsuario batoipopUsuarioByUsuarioEmisor) {
        this.batoipopUsuarioByUsuarioEmisor = batoipopUsuarioByUsuarioEmisor;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
