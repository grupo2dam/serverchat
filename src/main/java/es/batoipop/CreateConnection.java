package es.batoipop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class CreateConnection{
    private Socket cliente1Socket;
    private Socket cliente2Socket;
    private Cliente cliente1;
    private Cliente cliente2;
    private InOut inOut1;
    private InOut inOut2;

    public CreateConnection( Cliente cliente1, Socket cliente1Socket, Cliente cliente2 ,Socket cliente2Socket) {
        this.cliente1Socket = cliente1Socket;
        this.cliente2Socket = cliente2Socket;
        this.cliente1 = cliente1;
        this.cliente2 = cliente2;
        createConnexion();
    }

    public Cliente getCliente1() {
        return cliente1;
    }

    public Cliente getCliente2() {
        return cliente2;
    }

    private void createConnexion(){
        try {
            cliente1.setEnChatTrue();
            cliente2.setEnChatTrue();
            inOut1 = new InOut(new BufferedReader(new InputStreamReader(cliente2Socket.getInputStream())) ,new PrintWriter(cliente1Socket.getOutputStream(), true), cliente2, cliente1);
            inOut2 = new InOut(new BufferedReader(new InputStreamReader(cliente1Socket.getInputStream())), new PrintWriter(cliente2Socket.getOutputStream(), true), cliente1, cliente2);
            inOut1.start();
            inOut2.start();
            System.out.println("created connection between " + cliente1.getIdCliente() + " and " + cliente2.getIdCliente());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopInerThread(){
        inOut1.interrupt();
        inOut2.interrupt();
    }
}
