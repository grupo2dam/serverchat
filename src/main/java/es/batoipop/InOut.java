package es.batoipop;

import com.google.gson.Gson;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Date;

public class InOut extends Thread {
    private BufferedReader br;
    private PrintWriter pw;
    private Cliente cliente1;
    private Cliente cliente2;
    private EncryptionService encryptionService;
    private String message;

    public InOut(BufferedReader br, PrintWriter pw, Cliente cliente1, Cliente cliente2) {
        this.br = br;
        this.pw = pw;
        this.cliente1 = cliente1;
        this.cliente2 = cliente2;
        encryptionService = EncryptionService.getIntance();
        System.out.println("Conexion iniciada");
    }

    @Override
    public void run() {
        try {
            while ((message = br.readLine()) != null) {
                System.out.println("Mensaje: " + message);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message message1;
                        Gson gson = new Gson();
                        URL url;
                        HttpURLConnection con;
                        try {
                            String decrypted = encryptionService.decrypt(message);
                            message1 = gson.fromJson(decrypted, Message.class);
                            BatoipopMensajeUsuario mensajeUsuario = new BatoipopMensajeUsuario();
                            mensajeUsuario.setContenido(encryptionService.encrypt(message1.getContent()));
                            mensajeUsuario.setBatoipopUsuarioByUsuarioEmisor(cliente1.getUsuario());
                            mensajeUsuario.setBatoipopUsuarioByUsuarioReceptor(cliente2.getUsuario());
                            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                            Date date = new Date(System.currentTimeMillis());
                            mensajeUsuario.setFecha(formatter.format(date));

                            url = new URL("http://137.74.226.42:8080/mensajes");
                            con = (HttpURLConnection) url.openConnection();
                            con.setDoOutput(true);
                            con.setRequestMethod("POST");
                            con.setRequestProperty("Content-Type", "application/json");

                            String mensajeString = gson.toJson(mensajeUsuario, BatoipopMensajeUsuario.class);
                            OutputStream os = con.getOutputStream();
                            os.write(mensajeString.getBytes(StandardCharsets.UTF_8));
                            os.flush();

                            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            for (int c = in.read(); c != -1; c = in.read())
                                System.out.print((char) c);
                            System.out.println();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                pw.println(message);
                thread.start();
            }
            throw new IOException();
        } catch (IOException e) {
            System.out.println("conexion entre clientes cerrada de " + cliente1.getIdCliente() + " a " + cliente2.getIdCliente());
            cliente1.setEnChatFalse();
            cliente2.setEnChatFalse();
            cliente1.changeConectado(false);
            cliente2.changeConectado(false);
            cliente1.setSocket(null);
            cliente2.setSocket(null);
        }

    }

}
