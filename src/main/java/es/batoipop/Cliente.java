package es.batoipop;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

public class Cliente extends Thread {
    private int idCliente;
    private Socket socket;
    private Boolean conectado;
    private String cliente2;
    private Boolean enChat;
    private BatoipopUsuario usuario;

    public Cliente(Socket socket) {
        this.socket = socket;
        this.conectado = false;
        this.enChat = false;
    }

    public void setId() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            idCliente = Integer.parseInt(br.readLine());
            setUser();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUser() {
        String lin, salida = "";
        usuario = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url = new URL("http://137.74.226.42:8080/usuario?id=" + idCliente);
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                usuario = gson.fromJson(salida, BatoipopUsuario.class);
                con.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BatoipopUsuario getUsuario() {
        return usuario;
    }

    public void setEnChatFalse() {
        this.enChat = false;
    }

    public void setEnChatTrue() {
        this.enChat = true;
    }

    public void changeEnChat() {
        this.enChat = !enChat;
    }

    public void changeConectado(Boolean conectado) {
        this.conectado = conectado;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public Socket getSocket() {
        return socket;
    }

    public Boolean isConectado() {
        return conectado;
    }

    public String getCliente2() {
        return cliente2;
    }

    public Boolean isEnChat() {
        return enChat;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + idCliente +
                ", socket=" + socket +
                '}';
    }

    @Override
    public void run() {
        try {
            PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
            pw.println("conectado");
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            do {
                cliente2 = br.readLine();
                if (cliente2 != null) {
                    conectado = true;
                }
            } while (!conectado);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
