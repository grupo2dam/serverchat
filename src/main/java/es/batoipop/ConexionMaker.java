package es.batoipop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

public class ConexionMaker extends Thread {
    ArrayBlockingQueue<Cliente> clientes;
    ArrayBlockingQueue<CreateConnection> connections = new ArrayBlockingQueue<>(1024);

    public ConexionMaker(ArrayBlockingQueue<Cliente> clientes) {
        this.clientes = clientes;
    }

    @Override
    public void run() {
        do {
            comprobarConexionesCerradas();
            if (clientes.size() > 0) {
                for (Cliente cliente : clientes) {
                    if (cliente.isConectado() && !cliente.isEnChat()) {
                        String cliente2 = cliente.getCliente2();
                        Cliente cliente1 = null;
                        do {
                            cliente1 = checkClient(cliente2);
                        }while(cliente1 == null);
                        boolean conexionExiste = false;
                        for (CreateConnection connection : connections) {
                            if (connection.getCliente1().getIdCliente() == cliente.getIdCliente() && connection.getCliente2().getIdCliente() == cliente1.getIdCliente() ||
                                    connection.getCliente1().getIdCliente() == cliente1.getIdCliente() && connection.getCliente2().getIdCliente() == cliente.getIdCliente()){
                                conexionExiste = true;
                            }
                        }
                        if (!conexionExiste && cliente1.getSocket() != null && cliente.getSocket() != null){
                            CreateConnection c = new CreateConnection(cliente, cliente.getSocket(), cliente1, cliente1.getSocket());
                            cliente.changeConectado(true);
                            cliente1.changeConectado(true);
                            connections.add(c);
                        }
                    }
                }
            }
        } while (true);
    }

    private void comprobarConexionesCerradas() {
        if (connections.size() > 0){
            for (CreateConnection connection : connections) {
                if (!connection.getCliente1().isEnChat() && !connection.getCliente2().isEnChat()){
                    System.out.println(connection.getCliente1().isEnChat() + " " + connection.getCliente2().isEnChat());
                    connection.stopInerThread();
                    connections.remove(connection);
                    System.out.println("Conexion eliminada");
                }
            }
        }
    }

    private Cliente checkClient(String cliente){
        Cliente cliente1 = null;
        if (cliente != null){
            for (Cliente cliente3 : clientes) {
                if (cliente3.getIdCliente() == Integer.parseInt(cliente)) {
                    cliente1 = cliente3;
                    return cliente1;
                }
            }
        }
        return cliente1;
    }
}
