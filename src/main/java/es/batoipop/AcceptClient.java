package es.batoipop;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

public class AcceptClient extends Thread {
    ArrayBlockingQueue<Cliente> clientes;
    ServerSocket serverSocket;

    public AcceptClient(ArrayBlockingQueue<Cliente> clientes, ServerSocket serverSocket) {
        this.clientes = clientes;
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        try {
            Cliente cliente = new Cliente(serverSocket.accept());
            cliente.setId();

            cliente.start();
            clientes.add(cliente);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ConexionMaker conexionMaker = new ConexionMaker(clientes);
        conexionMaker.start();
        /*CheckDisconection checkDisconection = new CheckDisconection(clientes);
        checkDisconection.start();*/
        do {
            try {
                Cliente cliente = new Cliente(serverSocket.accept());
                cliente.setId();
                Boolean existe = false;


                for (Cliente cliente1 : clientes) {
                    if (cliente1.getIdCliente() == cliente.getIdCliente()) {
                        existe = true;
                        clientes.remove(cliente1);
                        cliente.start();
                        System.out.println(cliente.getIdCliente() + " anadido de nuevo");
                        clientes.add(cliente);
                    }
                }

                if (!existe) {
                    cliente.start();
                    clientes.add(cliente);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (true);
    }


}
