package es.batoipop;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;

public class App {
    public static void main(String[] args) {
        ArrayBlockingQueue<Cliente> clientes = new ArrayBlockingQueue<>(1024);
        int portNumber = 9090;

        try{
            ServerSocket serverSocket = new ServerSocket(portNumber);
            AcceptClient acceptClient = new AcceptClient(clientes ,serverSocket);
            acceptClient.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
