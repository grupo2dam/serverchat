package es.batoipop;

import java.io.Serializable;

public class BatoipopUsuario implements Serializable {
    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private String contrasena;

    public BatoipopUsuario(int id, String nombre, String email, String telefono, String contrasena) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.contrasena = contrasena;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getContrasena() {
        return contrasena;
    }
}
