package es.batoipop;

import java.util.concurrent.ArrayBlockingQueue;

public class CheckDisconection extends Thread{
    ArrayBlockingQueue<Cliente> clientes;

    public CheckDisconection(ArrayBlockingQueue<Cliente> clientes) {
        this.clientes = clientes;
    }

    @Override
    public void run() {
        do {
            for (Cliente cliente : clientes) {
                if (!cliente.getSocket().isConnected()){
                    cliente.changeConectado(false);
                    cliente.setEnChatFalse();
                }
            }
        }while (true);
    }
}
